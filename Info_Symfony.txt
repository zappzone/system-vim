######################################
### 1.4  
######################################

php symfony doctrine:data-load

php symfony doctrine:build --all --and-load

php symfony doctrine:generate-module --with-show --non-verbose-templates frontend job JobeetJob


php symfony plugin:publish-assets
php symfony doctrine:build --all
php symfony doctrine:data-load





######################################
### 2.x  
######################################

==========================================
php app/console - Befehle für Symfony2 
==========================================


Install Symlinks
=====================================
php app/console assets:install 
--symlink web/


Update DB form Entity 
=====================================
php app/console doctrine:schema:update 
--dump-sql 
--force

Create SQL from a sectfic entity // managed in cofig 
========================================
php bin/console doctrine:schema:update --force --em=manager_remote



Clear Cache 
=====================================
php app/console cache:clear


Generate Bundle
=====================================
php app/console generate:bundle



Dumps yml languages  
=====================================
php app/console bcc:trans:update --dump-messages de EosMailBundle



Things I need
=====================================
generate:doctrine:crud                Generates a CRUD based on a Doctrine entity
generate:doctrine:entities            Generates entity classes and method stubs from your mapping information
generate:doctrine:entity              Generates a new Doctrine entity inside a bundle



Composer 
========================================================
composer create-project symfony/framework-standard-edition httpdocs



Symfony 3
========================================================
php bin/console server:run
php bin/console security:check

http://localhost:8000


php bin/console server:run 127.0.0.1:8001

