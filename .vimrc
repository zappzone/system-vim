" VIM Configuration by
" bjoern pfennig
"
" 


" my global settings 
set paste
set autoindent
set shiftwidth=2
set softtabstop=2
set tabstop=2
set hlsearch
syntax on 
set number

" for tabs 
map tt :tabnext<CR>
map tp :tabprevious<CR>
map te :tabedit 

" NERDTree CRTL + n
map <C-n> :NERDTreeToggle<CR>


" phpfolding.vim extension needs to have this option on
filetype plugin on

" to make commands zf,ze,ZE, etc ...run, set foldmethod to manual
" set foldmethod=manual

" let php_folding=0 
" set nofoldenable " disable folding
" let g:vim_markdown_folding_disabled=1
" set foldenable!


" Map Key for Extension phpfolding.vim
map <F5> <Esc>:EnableFastPHPFolds<Cr>
map <F6> <Esc>:EnablePHPFolds<Cr>
map <F7> <Esc>:DisablePHPFolds<Cr>


" new from http://vim.wikia.com/wiki/Example_vimrc

" Vim with default settings does not allow easy switching between multiple files
" in the same editor window. Users can use multiple split windows or multiple
" tab pages to edit multiple files, but it is still best to enable an option to
" allow easier switching between files.
"
" One such option is the 'hidden' option, which allows you to re-use the same
" window and switch from an unsaved buffer without saving it first. Also allows
" you to keep an undo history for multiple files when re-using the same window
" in this way. Note that using persistent undo also lets you undo in multiple
" files even in the same window, but is less efficient and is actually designed
" for keeping undo history after closing Vim entirely. Vim will complain if you
" try to quit without saving, and swap files will keep you safe if your computer
" crashes.
set hidden

" Note that not everyone likes working this way (with the hidden option).
" Alternatives include using tabs or split windows instead of re-using the same
" window as mentioned above, and/or either of the following options:
" set confirm
" set autowriteall
  
" Better command-line completion
set wildmenu
	 
" Show partial commands in the last line of the screen
set showcmd

" 
" Create or Customize Commands
"  
"





au WinEnter * set nofen
au WinLeave * set nofen

set foldmethod=indent 
set foldlevel=20
set foldlevelstart=20 
set nofoldenable
let g:vim_markdown_folding_disabled=1
