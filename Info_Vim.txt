VIM WICHTIGES
###############################

# Befehle 
#============================================================

/                   => suchen
CTRL-*              => markieren und suchen 
ALT+#               => Wort markieren  
n                   => suche das n�chste markeiert 

esc                 => Aktion verlassen

dd        	        => l�schen
u            	    => R�ckg�ngig
yy                  => Kopieren 
p                   => Einf�hen 

v                   => visual 
SHIFT-CTRL-v        => visual block            

CTRL-n              => Stichwort-Erg�nzung => Vorschlag: Variablennamen     
CTRL-p              => Stichwort-Erg�nzung => Vorschlag: Variablennamen 

Einr�cken: 
SHIFT-= %            => Auf Klammer gehen Einr�cken
SHIFT-= =-G          => R�cke die gesamte Datei ein 
=G 		              => Einr�cken f�r alle

Strg-v              => Block markieren 
>                   => nach rechts einr�cken 
<                   => nach links einr�cken

gg		            => springe zur ersten Zeile
G                   => springe zur letzten Zeile 

:Zahl               => springe in die Zeile X    
:1                  => springe in die erste Zeile

STR-%               => springe von Klammer zu Klammer 

:p!                 => nichts speichern und verlassen
:wq                 => speichern und verlassen
:x                  => speichern und verlassen 

:sy on              => Syntax highlight on
:sy off             => Syntax highlight off 

:%s/.../.../        => RegEx: suche und ersetze 
:%s/.../.../g       => RegEx: suche und ersetze im gesamten Text 

:colorscheme XXX    => setzt die syntax highlight color 

:set paste          =>
:set autoindent     =>
:set smartindent    =>
:set shiftwidth=2   =>
:set tabstop=2      =>
:set expandtab      =>
:set bs=2           => Schalte die Backspace funktion an
:set hlsearch       => schaltet die highlight suche an 
:nohlsearch         => schaltet die highlight such aus 


:set showmatch      => zeige die zugeh�rigen klammern an
%                   => springe zur klammer


# SPLIT SCREEN
#===============================================================
vi dat1 dat2 
:sb2                => (splitt buffer)

ODER
vi dat1
:sp dat2

Horizontal splitten: :split
Vertikal splitten: :vsplit
Als Tastatur-K�rzel:

    Strg-w s: Horizontal splitten
    Strg-w v: Vertikal splitten
    Strg-w w: zum n�chsten Fenster springen
    Strg-w +: aktuelles Fenster gr�sser machen
    Strg-w -: aktuelles Fenster kleiner machen
    Strg-w _: aktuelles Fenster maximieren
    Strg-w =: Fenstergr�ssen gleichm�ssig verteilen
    Strg-w c: Fenster schliessen


# VIMDIFF 
#============================================================
vimdiff conf/config.inc.php3 www/support/muster.config.inc.php3

WICHTIGES: 
- wenn du �nderungen rechts einspielen m�chtest, befinde dich auf der linken Seite
- Befinde dich UNTER der �nderung, wenn du sie �bernehmen m�chtest

CTRL-ww   	        => Seiten der Dateien wechseln
dp            	    => �nderung �bernehmen  ()
:diffupdate         => Update das DIff und Zeige die �nderugen


diff -b -w -B www/support/muster.config.inc.php3 config_FUENF-1.inc.php3 | vi -


# F�R .vimrc
#======================================================================



